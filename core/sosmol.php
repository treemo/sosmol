<?php

include_once 'Post.class.php';
include_once 'ShortPost.class.php';
include_once 'smarty/Smarty.class.php';

class Sosmol {

	private static $_instance;
	
	public function __construct(){
		$this->config = parse_ini_file('config.ini');
		$this->config['theme_url'] = $this->config['base_url'].'/themes/'.$this->config['theme'];

		$this->tpl = new Smarty();
		$this->tpl->addTemplateDir('themes/global/');
		$this->tpl->addTemplateDir('themes/'.$this->config['theme']);
		$this->tpl->setCompileDir($this->config['cache_dir'].'/smarty/compiled');
		if($this->config['cache'] == 1){

		}
	}


	public static function getInstance()
	{
		if(!self::$_instance)
		{
			self::$_instance = new self();
		}

		return self::$_instance;
	}


	public function init()
	{
		$this->tpl->assign('config', $this->config);

		if(isset($_GET['uri']) && preg_match('/^\/(\d{4})\/(\d{2})\/(\d{2})\/([^\.\/]+)/i', $_GET['uri'], $matches))
		{
			$this->post = new Post($matches[1],$matches[2],$matches[3],$matches[4], $this->config['base_url'], $this->config['posts_dir']);

			$this->tpl->assign('post', $this->post);
			$this->tpl->assign('file', 'post.tpl');

			$result = $this->tpl->fetch('layout.tpl');
		} 
		else 
		{
			/* We retrieve all the post */
			$posts = array();
			foreach (array_reverse(glob("posts/*.md")) as $post) {
				if(preg_match('/posts\/(\d{4})-(\d{2})-(\d{2})-([^\.\/]+).md/i', $post, $matches)){
					$posts[] = new ShortPost($matches[1],$matches[2],$matches[3],$matches[4], $this->config['base_url'], $this->config['posts_dir']);
				}
			}

			/* If the user request the RSS stream */
			if(isset($_GET['uri']) && ($_GET['uri'] == '/rss'))
			{
				header("Content-type: application/rss+xml");
				$this->tpl->assign('posts', $posts);
				$result = $this->tpl->fetch('rss.tpl');

			/* If the user want to list the articles */
			} else if(isset($_GET['uri']) && ($_GET['uri'] == '/list'))
			{
				$this->tpl->assign('posts', $posts);
				$this->tpl->assign('file', 'list.tpl');

				$result = $this->tpl->fetch('layout.tpl');
				
			/* If no arguments, we display the blog */
			} else {
				/* Pagination */
				$currentPage = isset($_GET['page'])? $_GET['page'] : 1;
				$posts = array_chunk($posts, $this->config['posts_per_page']);
				$nbPage = count($posts);

				$posts = $posts[$currentPage-1]; 

				$this->tpl->assign('nbPage', $nbPage);
				$this->tpl->assign('currentPage', $currentPage);
				$this->tpl->assign('posts', $posts);
				$this->tpl->assign('file', 'home.tpl');

				$result = $this->tpl->fetch('layout.tpl');
			}
		}
		echo $result;

		if($this->config['cache'] == 1){
			if(isset($_GET['uri'])){
				$file = preg_replace('#/#', '', $_GET['uri']).'.html';
			} else {
				$file = 'index.html';
			}
			file_put_contents($this->config['cache_dir'].'/'.$file, $result); 
		}
	}

}
