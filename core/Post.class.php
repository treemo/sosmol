<?php

include_once "michelf/Markdown.php";

class Post {

	protected $year;
	protected $month;
	protected $day;
	protected $timestamp;

	protected $titleURL;
	protected $filename;

	protected $title;
	protected $content;
	
	public function __construct($year, $month, $day, $title, $base_url){
		$this->year = $year;
		$this->month = $month;
		$this->day = $day;
		$this->url = "$base_url/$year/$month/$day/$title";	
		$this->filename = "$year-$month-$day-$title.md";

		$this->timestamp = strtotime("$day-$month-$year");

		$this->parseArticle();
	}

	public function parseArticle(){
		$fp = fopen("posts/$this->filename",'r');
		if(!$fp) return;

		$this->title = preg_replace('/title: (\w+)/', '${1}', fgets($fp));

		$this->content = "";
		while (($buffer = fgets($fp)) !== false){
			if(strpos($buffer, "[SEPARATOR]") === false){
				$this->content .= $buffer;
			}
		}

		$this->content = $this->parseMD($this->content);
		fclose($fp);
	}

	public function parseMD($content){
		#return Markdown($content);
		return MarkdownExtra::defaultTransform($content);
	}

	public function __get($name){
		if(isset($this->$name)){
			return $this->$name;
		}
		return "";
	}
}
